class ListaDuplamenteEncadeada {
    constructor(head = null, tail = null, count = 0){
        this.head = head;
        this.tail = tail;
        this.count = count;
    }

    MostrarTudo(){
        if (this.head){
            let arr = [];
            let current = this.head;
            for (let i = 0; i < this.count; i++){
                arr[i] = current.data;
                current = current.next;
            }
            return arr;
        } else {
            return null;
        }
    }

    AdicionarInicio(data){
        let no = new Node(data);
        no.next = this.head;
    
        this.head = no;
    
        if (this.count === 0){
            this.tail = this.head;
        } else {
            this.head.next.previous = this.head;
        }
        this.count++;
    }

    getHead(){
        if (this.head) {
            return this.head.data;
        }
        return null;
    }
    getTail(){
        if (this.tail) {
            return this.tail.data;
        }
        return null;
    }
    GetCount(){
        return this.count;
    }
}

class Node{
    constructor(data, next = null, previous = null){
        this.data = data;
        this.next = next;
        this.previous = previous;
    }
}

let lista = new ListaDuplamenteEncadeada();
lista.AdicionarInicio(1);
lista.AdicionarInicio(4);
lista.AdicionarInicio(5);
lista.AdicionarInicio(6);

console.log(lista.MostrarTudo());







