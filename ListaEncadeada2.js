function LinkedList(){
    let head = null
    let length = 0
    
    const Node = (value) =>{
        return{
            value,
            next: null
        }
    }

    const add = (value) => {
        if(!head){
            head = Node(value)
            length++
            return head
        }
        let node = head
        while(node.next){
            node = node.next
        }
       
        node.next = Node(value)
        length++
        return node.next
        
    }

    return {
        length: () => length,
        add: (value) => add(value),
    }
}

const list = LinkedList()
console.log(list.length())
console.log(list.add(1))
console.log(list.add(2))
console.log(list.add(3))